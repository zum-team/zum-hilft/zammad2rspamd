/*
mod.ts: zammad2rspamd - let rspamd learn from zammad tickets

Copyright (C) 2021 by Christian Schröder <public@l7010.de>
*/

async function printUsage() {
  await Deno.stderr.write(new TextEncoder().encode(
    `usage: zammad2rspamd ZAMMAD_ENDPOINT ZAMMAD_CREDENTIAL_FILE RSPAMC_ENDPOINT RSPAMC_CREDENTIAL [LIMIT]

Positional Arguments:

  ZAMMAD_ENDPOINT           URL of the Zammad API to talk to (e.g. https://zammad.local/api)
  ZAMMAD_CREDENTIAL_FILE    Location of a readable file to read the Zammad API token from
  RSPAMC_ENDPOINT           Endpoint for rspamc
  RPSAMC_CREDENTIAL_FILE    Location of a readable file to read the rspamc credential from
  LIMIT                     (optional: default is 1) Number of tickets to process

zammad2rspamd will fetch not yet processed ticket articles to feed rspamd with ham and spam.

ham are ticket articles which where auto-detected as spam but have no spam tag anymore. 
That implies, they were manually corrected to be non-spam.
The query used is: '${QUERIES.ham}'

spam are ticket articles which where not autodetected as spam but have a spam tag now. 
That implies, they were manually corrected to be spam.
The query used is: '${QUERIES.spam}'

After processing an article the ticket will be tagged with zammad2rspamd as processed to prevent it from being learned multiple times.

`,
  ));
}

import Papyrus from "https://deno.land/x/papyrus@v1.0.0/mod.ts";

const logger = new Papyrus({
  useLabels: true,
});

const QUERIES = {
  "ham": "!tags:zammad2rspamd && !tags:spam &&  tags:autospam",
  "closed_ham":
    "!tags:zammad2rspamd && !tags:autospam && !tags:spam && state.name: closed",
  "spam": "!tags:zammad2rspamd && tags:spam && !tags:autospam",
};
const RSPAMD_MODE: Record<keyof typeof QUERIES, string> = {
  "ham": "learn_ham",
  "closed_ham": "learn_ham",
  "spam": "learn_spam",
};

type ZammadResponse = {
  assets: Record<ZammadObjectType, Record<ZammadObjectId, ZammadObject>>;
  result: ZammadResponseItem[];
};

type ZammadObjectType = "Article" | "Ticket";
type ZammadObjectId = string;

type ZammadResponseItem = {
  type: ZammadObjectType;
  id: ZammadObjectId;
};
type ZammadObject = ZammadResponseItem & Record<string, unknown>;

type ZammadTicket = ZammadObject & {
  type: "Ticket";
  article_ids: ZammadObjectId[];
};

type Zammad2RspamdCtx = {
  token: string;
  zammadEndpoint: string;
  rspamdEndpoint: string;
  rspamdPassword: string;
  mode: keyof typeof QUERIES;
  limit: number;
};

async function callZammad(
  ctx: Zammad2RspamdCtx,
  apiEndpoint: string,
  r?: RequestInit,
): Promise<Response> {
  const headers = new Headers(r?.headers);
  headers.set("Authorization", `Token ${ctx.token}`);
  if (!headers.has("Content-Type")) {
    headers.set("Content-Type", "application/json");
  }
  try {
    return await fetch(`${ctx.zammadEndpoint}${apiEndpoint}`, {
      headers,
      ...r,
    });
  } catch (error) {
    logger.warn("error on zammad call", { error });
    throw error;
  }
}

async function findTickets(
  ctx: Zammad2RspamdCtx,
  query: string,
): Promise<ZammadTicket[]> {
  const r = await callZammad(
    ctx,
    `/v1/search?${new URLSearchParams({
      query,
      limit: ctx.limit.toString(10),
    })}`,
  );
  const response = await r.json() as ZammadResponse;
  return response.result.filter((resultItem) => resultItem.type === "Ticket")
    .map((resultItem) =>
      response.assets[resultItem.type][resultItem.id] as ZammadTicket
    );
}

async function processArticleText(
  ctx: Zammad2RspamdCtx,
  articleText: string,
  ticketId: ZammadObjectId,
): Promise<void> {
  const rspamc = Deno.run({
    cmd: [
      "rspamc",
      "--connect",
      ctx.rspamdEndpoint,
      "--password",
      ctx.rspamdPassword,
      RSPAMD_MODE[ctx.mode],
    ],
    stdin: "piped",
    stderr: "piped",
    stdout: "piped",
  });
  if (!rspamc.stdin || !rspamc.stderr) {
    Deno.exit(50);
  }
  await rspamc.stdin.write(new TextEncoder().encode(articleText));
  rspamc.stdin.close();
  rspamc.stderrOutput().then((err) =>
    logger.warn("rspamc stderr", {
      stderrOutput: new TextDecoder().decode(err),
    })
  );
  rspamc.output().then((out) =>
    logger.debug("rspamc stdout", { stdout: new TextDecoder().decode(out) })
  );
  const status = await rspamc.status();
  if (status.success) {
    await callZammad(ctx, `/v1/tags/add`, {
      body: JSON.stringify({
        "item": "zammad2rspamd",
        "object": "Ticket",
        "o_id": ticketId,
      }),
      method: "POST",
    });
    return;
  } else {
    return Promise.reject("error from rspamc");
  }
}

async function processArticle(
  ctx: Zammad2RspamdCtx,
  ticketid: ZammadObjectId,
  articleid: ZammadObjectId,
): Promise<void> {
  logger.info("processing article", { ticketid, articleid });
  const r = await callZammad(ctx, `/v1/ticket_article_plain/${articleid}`);
  const articleText = await r.text();
  return processArticleText(ctx, articleText, ticketid);
}

function processTicket(
  ctx: Zammad2RspamdCtx,
  ticket: ZammadResponseItem & Record<string, unknown> & {
    type: "Ticket";
    article_ids: ZammadObjectId[];
  },
) {
  logger.info("processing ticket", { id: ticket.id });
  return processArticle(
    ctx,
    ticket.id,
    ticket.article_ids[ticket.article_ids.length - 1],
  );
}

async function process(ctx: Zammad2RspamdCtx): Promise<void[]> {
  const tickets = await findTickets(ctx, QUERIES[ctx.mode]);
  return Promise.all(
    tickets.map((ticket) =>
      processTicket(ctx, ticket)
        .catch((error) => {
          logger.warn("error processing ticket", { id: ticket.id, error });
          return Promise.resolve();
        })
    ),
  );
}

async function main(args: string[]): Promise<void> {
  if (args.length < 4 || args.length > 5) {
    await printUsage();
    Deno.exit(3);
    return;
  }
  const ctxInit = {
    zammadEndpoint: args[0],
    token: new TextDecoder().decode(Deno.readFileSync(args[1])),
    rspamdEndpoint: args[2],
    rspamdPassword: new TextDecoder().decode(Deno.readFileSync(args[3])),
    limit: Number.parseInt(args[4] || "1", 10),
  };

  const ham = process({ mode: "ham", ...ctxInit });
  const spam = process({ mode: "spam", ...ctxInit });
  const closed_ham = process({ mode: "closed_ham", ...ctxInit });
  return Promise.all([ham, spam, closed_ham]).then((_ignored) => {
    return;
  });
}

try {
  await main(Deno.args);
} catch (e) {
  logger.error("error encountered", { error: e });
  Deno.exit(10);
}
